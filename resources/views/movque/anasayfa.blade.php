@extends('movque.master')
@section('content')

	<div class="tanit tanit1">
			<div class="container col-md-12 icerik text-center" style="box-sizing: inherit;">
			
					<img src="{{url('/img/mavqueTEAM.gif')}}" height="100px" style="margin-bottom:3%">
					<p>Movque ile projelenizde mükemmel sonuçlara ulaşacaksınız!</p>
			</div>
	</div>
	
	<div class="tanit gradient1" >

		<div class="container col-md-12 icerik text-center" style="margin-top: -3%;">
				<img src="{{url('/img/tanim.gif')}}"  height="400px" style="margin-bottom:3%; border-radius: 5px; ">
				<p>Projelerinizi baştan sona takip edin</p>
		</div>
	</div>
	<div class="tanit gradient2" >

		<div class="container col-md-12 icerik text-center" style="margin-top: -20%;">
				<img src="{{url('/img/screen.png')}}"   height="500px" style="margin-top: 11%;">
				<p style="margin-top: -3%;">Görevler, projeler, konuşmalar ve gösterge tablolarıyla Movque, ekiplerin işi başından sonuna taşımasına olanak tanır.</p>
		</div>
	</div>
	<div class="tanit2 gradient3" >

		<div class="container col-md-12 icerik1 text-center" style="font-size:20px;">
			<form class="form-horizontal">
			  <div class="form-group">
			    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
			    <div class="col-sm-10 mm">
			      <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
			    </div>
			    <button type="submit" class="btn btn-default" style="margin-top: 0.5%;">Sign in</button>
			  </div>

			</form>
		</div>
	</div>

@endsection