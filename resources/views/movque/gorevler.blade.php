@extends('movque.master')
@section('content')
<div class="container">
	<div class="col-md-8">
	 <!-- Görev Listele -->
		<div class="panel panel-danger">
			<div class="panel-heading"><i class="fa fa-cube" aria-hidden="true"></i> {{@$proje->project_name}} 
			@if(Auth::user()->role_id === 2 || (Auth::user()->role_id === 1 && Auth::user()->id === $proje->modID))
			<a href="" data-toggle="modal" data-target="#projeAyar" class="pull-right ayar"><i class="fa fa-cog aria-hidden="true"></i></a>
					<!-- AYAR Modal -->
					<div class="modal fade" id="projeAyar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
						    <div class="modal-content">
							    <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        <h4 class="modal-title" id="myModalLabel">Proje Düzenle</h4>
							    </div>
							    <div class="modal-body">
							        <!-- Burası Modal İçerik Kısmı -->
							            <form class="form-horizontal" id="projeformguncelle" method="post" action="{{url('/projeler/'.@$proje->project_id)}}">
							                <input type="hidden" name="_token" value="{{ csrf_token() }}">
							                <input type="hidden" name="project_id" value="{{ @$projeguncelle -> project_id }}">
										  <div class="form-group has-error">
											  <label class="col-sm-2 control-label">Proje Adı:</label>
											  <div class="col-sm-10">
											      <input type="name" name="proje" value="{{@$proje->project_name}}" class="form-control">
											  </div>
										  </div>
										  <div class="form-group has-error">
											  <label class="col-sm-4 control-label">Proje Tamamlandı mı?</label>
											  <div class="col-sm-8">
													<select name="goster" class="form-control">
													  <option value="0">Hayır</option>
													  <option value="1">Evet</option>
													</select>
											  </div>
										  </div>


											<div class="form-group has-error">
											  <label class="col-sm-4 control-label">Proje Moderatörü:</label>
											  <div class="col-sm-8">
													<select name="modID" class="form-control">
													@foreach($modlist as $key => $value)
													  
													  @if(@$proje->modID === $value->id)
													  	<option value="{{ $value->id }}" selected>{{ $value->username }}</option>
													  @else
													  	<option value="{{ $value->id }}">{{ $value->username }}</option>
													  @endif


													@endforeach
													</select>
											  </div>
										  </div>


							        <!-- Burası Modal İçerik Kısmı Bitişi -->
							    </div>
							    <div class="modal-footer">
							        <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
							        <button type="submit" class="btn btn-danger">Güncelle</button>
							    </div></form>

						    </div>
						</div>
					</div> 	    @endif
					<!-- AYAR Modal Biter -->
			</div>
			<!-- Table -->
				<table class="table">
					<thead>
						<tr>
							<td class="col-md-8">
@if(Auth::user()->role_id === 2 || (Auth::user()->role_id === 1 && Auth::user()->id === $proje->modID))
								<form id="form" method="post" action="{{url('/gorevolustur')}}">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">	
									<input type="hidden" value="{{ @$proje->project_id }}" name="project_id">
										<s>
										<div class="input-group pull-left" style="margin-bottom: 5px;">
										  <span class="input-group-addon"><i class="fa fa-tasks" aria-hidden="true"></i></span>
										  <input class="form-control" type="text" name="gorev[]" placeholder="Görev Tanımla">
										  
										</div>
										</s>	
										<button type="submit" class="btn btn-danger pull-left" style="position: absolute; margin-left: 10px;">Gönder</button>
								</form>
							</td>
							<td class="col-md-4">
								<button id="button" class="btn btn-danger pull-right">Ekle</button>
								<button id="buttons" class="btn btn-danger pull-right" style="margin-right: 5px">Tümünü Sil</button>
							</td>
						</tr>
						<tr>
							<th>Görevler</th>
							<th><i class="fa fa-check-square-o" aria-hidden="true"></i>  <input type="checkbox" class="pull-right" onClick="toggle(this)" /> Tümünü Seç</th>
						</tr>
					</thead>
					@endif
					<tbody>
							@foreach($projeler as $key => $value)
		<tr>
			<td class="td"><a href="{{url('/gorevsil/'.$value->id)}}" class="gorevsil" style="display: none"><i style="color: #fff; " class="fa fa-trash"  aria-hidden="true"></i></a><gorev>
			 {{$value->gorev}} 
			 </gorev></td>
					<td> 

@if(Auth::user()->role_id === 2 || (Auth::user()->role_id === 1 && Auth::user()->id === $proje->modID))
					<input class="pull-right" name="foo" type="checkbox" aria-label="..."></td>
@endif

		</tr>
		@endforeach
					</tbody>
				</table>
				<div class="panel-footer">Presented by: Team MOVQ</div>
		</div>
	</div>
	@foreach($mod as $key => $md)
	<div class=" col-md-4">
			<div class="sidebarprofil">
					<div class="pcover"></div>
					<img src="{{url($md->profilePicture)}}" alt="K.E.P." class="pavatar img-circle pull-left">
					<div class="pname">
						<i class="fa fa-star" aria-hidden="true" style="color: #f0ad4e;"> <a href="{{url('profilgoster/'.$md->username)}}"></i><span> {{$md->name." ".$md->surname}}</span></a>
						<br>
						<span class="text-muted large" >
						<em>  
						@if($md->role_id == 0)
						Kullanıcı
						@elseif($md->role_id == 1)
						Moderator
						@else
						Admin
						@endif
						</em>
						</span>
						<span class="text-muted small"><em>  {{ "@".$md->username }}</em></span>
					</div>
			</div>
	@endforeach
			<!-- <div class="list-group">
				<div class="projects panel-heading text-uppercase"><i class="fa fa-cubes" aria-hidden="true"></i> Projects</div>
				    <a href="#" class="list-group-item">
				        <i class="fa fa-file-text" aria-hidden="true"></i> Proje 1
				        <span class="pull-right text-muted small"><em>4 minutes ago</em>
				        </span>
				    </a>
				    <a href="#" class="list-group-item">
				        <i class="fa fa-file-text" aria-hidden="true"></i> Proje 2
				        <span class="pull-right text-muted small"><em>12 minutes ago</em>
				        </span>
				    </a>
				    <a href="#" class="list-group-item">
				        <i class="fa fa-file-text" aria-hidden="true"></i> Proje 3
				        <span class="pull-right text-muted small"><em>27 minutes ago</em>
				        </span>
				    </a>
				    <a href="#" class="list-group-item">
				        <i class="fa fa-file-text" aria-hidden="true"></i> Proje 4
				        <span class="pull-right text-muted small"><em>43 minutes ago</em>
				        </span>
				    </a>
				    <a href="#" class="list-group-item">
				        <i class="fa fa-file-text" aria-hidden="true"></i> Proje 5
				        <span class="pull-right text-muted small"><em>11:32 AM</em>
				        </span>
				    </a>
				    <a href="#" class="list-group-item">
				        <i class="fa fa-file-text" aria-hidden="true"></i> Proje 6
				        <span class="pull-right text-muted small"><em>11:13 AM</em>
				        </span>
				    </a>
				    <a href="#" class="list-group-item">
				        <i class="fa fa-file-text" aria-hidden="true"></i> Proje 7
				        <span class="pull-right text-muted small"><em>10:57 AM</em>
				        </span>
				    </a>
				    <a href="#" class="list-group-item">
				        <i class="fa fa-file-text" aria-hidden="true"></i> Proje 8
				        <span class="pull-right text-muted small"><em>9:49 AM</em>
				        </span>
				    </a>
				    <a href="#" class="list-group-item">
				        <i class="fa fa-file-text" aria-hidden="true"></i> Proje 9
				        <span class="pull-right text-muted small"><em>Yesterday</em>
				        </span>
				    </a>
			</div> -->
	</div>
</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
			<script type="text/javascript">
			$( ".td" ).mouseover(function() {
  				
  				$(this).find('a').show();
			});
			$( ".td" ).mouseleave(function() {
  				
  				$(this).find('a').hide();
			});
		</script>
	<script>
	var newLi;
	$(document).ready(function(){
	    $('#button').click(function(){
	    	 newLi = '<s>'+
	    	 '<div class="input-group x" style="margin-bottom: 5px;">'+
	    	 '<span class="input-group-addon"><i class="fa fa-tasks" aria-hidden="true"></i></span>'+
	    	 '<input class="form-control" type="text" name="gorev[]" placeholder="Görev Tanımla">'+
	    	 '<span style="background:#c83834" class="input-group-addon" onclick="sil(this)"><i style="color:white" class="fa fa-minus"  aria-hidden="true"></i></span>'+
	    	 '</div>';
	        $('#form').append(newLi);
	    });

	    $('#buttons').click(function(){
	      $('.x').closest("s").remove();
	    });
	});
	function sil(e){

		$(e).closest("s").remove();
	}
	function toggle(source) {
  checkboxes = document.getElementsByName('foo');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}

	</script>
@endsection