@extends('movque.master')
@section('content')
<div class="container">
  <div class="row">
        @if(count($admin) > 0)
          <div class="page-header">
            <h1><small>Adminler</small></h1>
          </div>
          @endif
      <!-- Kullanıcı Listele -->
           @foreach($admin as $uye)
           <div class="col-md-4">
            <div class="panel panel-danger">
              <div class="kullaniciduzenle_cover" style="background-image: url('{{ $uye->coverPicture  }}')"><img src="{{$uye->profilePicture}}" class="img-circle kullaniciduzenle_avatar"></i></div>
                <div class="kullaniciduzenle_heading text-capitalize"><a href="{{url('profilgoster/'.$uye->username)}}"> {{$uye->username}}</a> - Admin
                  <a href="/kullaniciduzenle/'.$uye->id" data-toggle="modal" data-target="#projeAyar" class="pull-right ayar"><i class="fa fa-cog aria-hidden="true"></i></a>
                </div> 
              </div>
            </div>
            @endforeach
  </div>
  <div class="row">

            @if(count($moderator) > 0)
          <div class="page-header">
            <h1><small>Moderatörler</small></h1>
          </div>
          @endif
      <!-- Kullanıcı Listele -->
           @foreach($moderator as $uye)
           <div class="col-md-4">
            <div class="panel panel-danger">
              <div class="kullaniciduzenle_cover" style="background-image: url('{{ $uye->coverPicture  }}')"><img src="{{$uye->profilePicture}}" class="img-circle kullaniciduzenle_avatar"></i></div>
                <div class="kullaniciduzenle_heading text-capitalize"><a href="{{url('profilgoster/'.$uye->username)}}"> {{$uye->username}}</a> - Moderatör
                  <a href="/kullaniciduzenle/'.$uye->id" data-toggle="modal" data-target="#projeAyar" class="pull-right ayar"><i class="fa fa-cog aria-hidden="true"></i></a>
                </div> 
              </div>
            </div>
            @endforeach
  </div>
  <div class="row">
            @if(count($uyeler) > 0)
          <div class="page-header">
            <h1><small>Üyeler</small></h1>
          </div>
          @endif
      <!-- Kullanıcı Listele -->
           @foreach($uyeler as $uye)
           <div class="col-md-4">
            <div class="panel panel-danger">
              <div class="kullaniciduzenle_cover" style="background-image: url('{{ $uye->coverPicture  }}')"><img src="{{$uye->profilePicture}}" class="img-circle kullaniciduzenle_avatar"></i></div>
                <div class="kullaniciduzenle_heading text-capitalize"><a href="{{url('profilgoster/'.$uye->username)}}"> {{$uye->username}}</a> - Üye
                  <a href="/kullaniciduzenle/'.$uye->id" data-toggle="modal" data-target="#projeAyar" class="pull-right ayar"><i class="fa fa-cog aria-hidden="true"></i></a>
                </div> 
              </div>
            </div>
            @endforeach
  </div> 
</div>
@endsection
