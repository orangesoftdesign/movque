<!DOCTYPE html>
<html>
<head>
	<title>Movque - Task Maneger</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="{{url('img/movque.ico')}}" type="image/ico" sizes="16x16">
  	<!-- Latest compiled and minified CSS -->
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  	<!-- Optional theme -->
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  	<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <!-- First include jquery js -->
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>



  	<!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Signika" rel="stylesheet">

</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" style="padding: 5px;" href="{{url('/')}}"><img src="{{url('img/movque.png')}}" height="40px;"></a>
      <p class="navbar-text">Task Manager</p>
    </div>
<form class="navbar-form pull-right">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search">
          <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search" aria-hidden="true"></i></span>
        </div>
        <button type="submit" style="visibility: hidden; margin-right: -90px;" class="btn btn-default">Submit</button>
      </form>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        @if(Auth::check())

            @if(Auth::user()->role_id == 2)
        <li><a href=""  data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i> Yeni Proje Oluştur</a></li>
            @endif
                  <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
  <img src="{{asset('/'.Auth::user()->profilePicture)}}" alt="K.E.P." class="img-circle profilavatarmaster"> {{Auth::user()->username }}<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{url('/profil')}}"> Profil</a></li>
            @if(Auth::user()->role_id == 2)
            <li><a href="{{url('/kullaniciduzenle')}}"> Kullanıcıları Düzenle</a></li>
             @endif
            <li role="separator" class="divider"></li>
            <li><a href="{{url('/logout')}}"><i class="fa fa-sign-out" aria-hidden="true"></i> Çıkış Yap</a></li>
          </ul>
        </li>
        @else
          <li><a href="{{url('/register')}}"><i class="fa fa-sign-in" aria-hidden="true"></i> Kaydol</a></li>
          <li><a href="{{url('/login')}}"><i class="fa fa-sign-in" aria-hidden="true"></i> Giriş Yap</a></li>
        @endif

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Proje Oluştur</h4>
      </div>
      <div class="modal-body">
        <!-- Burası Modal İçerik Kısmı -->
            <form class="form-horizontal" id="projeform" method="post" action="{{url('/projeler')}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group has-error">
                  <label class="col-sm-3 control-label">Proje İsmi: </label>
                  <div class="col-sm-9">
                    <input type="name" name="proje" class="form-control" placeholder="Proje ismi yazınız...">
                  </div>
                </div>
              <div class="form-group has-error">
                        <label class="col-sm-2 control-label">Proje Moderatörü:</label>
                        <div class="col-sm-10">
                          <select name="modID" class="form-control">

              @foreach(\App\User::get() as $value)
                  @if($value->role_id === 1)
                              <option value="{{ $value->id }}">{{ $value->username }}</option>
                  @endif

              @endforeach
                            
                          </select>
                        </div>
                      </div>



        <!-- Burası Modal İçerik Kısmı Bitişi -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
        <button type="submit" class="btn btn-danger">Oluştur</button>
      </div></form>
    </div>
  </div>
</div>
@yield('content')
</body>
</html>

