@extends('movque.master')
@section('content')
<div class="container"> 
<form method="POST" action="" enctype="multipart/form-data">
  <div class="profilcover" style="background-image: url('{{ Auth::user()->coverPicture  }}')"><img src="{{ Auth::user()->profilePicture  }}" alt="K.E.P." class="img-circle profilavatar"><br>
      <div class="fileboxProfile">
          <input type="file" name="image" style="visibility: hidden;" id="file-2" class="inputfile inputfile-3" data-multiple-caption="{count} files selected" multiple />
          <label for="file-2"> <i class="fa fa-upload" aria-hidden="true"></i> <span>Profil Fotoğrafı Seç</span></label>
      </div>
      <div class="filebox">
          <input type="file" name="imageK" style="visibility: hidden;" id="file-3" class="inputfile inputfile-3" data-multiple-caption="{count} files selected" multiple />
          <label for="file-3"> <i class="fa fa-upload" aria-hidden="true"></i> <span>Kapak Fotoğrafı Seç</span></label>
        </div>
  </div>
    <div class="col-md-10 pull-right form-horizontal formlocationinprofile">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="form-group has-error">
    <label class="col-sm-2 control-label">Adınız:</label>
    <div class="col-sm-10">
      <input type="name" name="name" value="{{ Auth::user()->name  }}" class="form-control" placeholder="John">
    </div>
  </div>
  <div class="form-group has-error">
    <label class="col-sm-2 control-label">Soyadınız:</label>
    <div class="col-sm-10">
      <input type="name" name="surname" value="{{ Auth::user()->surname  }}"  class="form-control" placeholder="DOE">
    </div>
  </div>
  <div class="form-group has-error">
    <label class="col-sm-2 control-label">Kullanıcıadı:</label>
    <div class="col-sm-10">
     <div class="input-group">
      <span class="input-group-addon" id="basic-addon1">@</span>
      <input type="name" name="username" value="{{ Auth::user()->username  }}"  class="form-control" placeholder="Kullanıcıadı">
      </div>
    </div>
  </div>
      <div class="form-group has-error">
      <label class="col-sm-2 control-label">E-mail Adresiniz:</label>
      <div class="col-sm-10">
        <input type="email" name="email" value="{{ Auth::user()->email  }}" class="form-control" placeholder="John">
      </div>
      </div>

  <div class="form-group navbar-right">

    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-danger">Kaydet</button>
    </div>

  </div>
    </form>
    <!-- Trigger the modal with a button -->
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#SifreDegistir">Şifreyi Değiştir</button>
<!-- Modal -->
<form method="POST" action="">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
<div id="SifreDegistir" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Şifre Değişikliği</h4>
      </div>
      <div class="modal-body">
        
        <div class="form-group has-error">
    <label class="col-sm-2 control-label">Mevcut Şifre:</label>
    <div class="col-sm-10">
      <input type="password" name="password" class="form-control" placeholder="Mevcut Şifre">
    </div>
    </div>

     <div class="form-group has-error">
    <label class="col-sm-2 control-label">Yeni Şifre:</label>
    <div class="col-sm-10">
      <input type="password" name="newpassword" class="form-control" placeholder="Yeni Şifre">
    </div>
    </div>

          <div class="form-group has-error">
              <label class="col-sm-2 control-label">Yeni Şifre (Tekrar):</label>
              <div class="col-sm-10">
                  <input type="password" name="newpasswordrepeat" class="form-control" placeholder="Yeni Şifre">
              </div>
          </div>


      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger">Kaydet</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
      </div>
    </div>

  </div>
</div>
</form>


</div>
</div>
@endsection