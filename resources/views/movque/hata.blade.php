@extends('movque.master')
@section('content')
  <div class="container">
      <img class="center-block" src="{{url('img/hata.png')}}" width="350" alt="">
      <h1 class="text-center">404 Error !</h1>
      <p class="text-center">Sanırım burada senin işin yok defol!</p>
      <p class="text-center"><a href="javascript:history.go(-1)">Önceki Sayfa</a> <strong>YADA</strong> <a href="#">Anasayfa'ya</a> gidebilirsin.</p>
  </div>
@endsection