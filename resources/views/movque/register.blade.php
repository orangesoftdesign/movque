@extends('movque.master')
@section('content')
<div class="container">
<div class="panel ppanel panel-danger col-md-5 kgiris kayitw ">
    <div class="panel-heading"><i class="fa fa-cube" aria-hidden="true"></i> KAYIT OL </div>
	    <div class="panelbody pkayit">
			<form class="form-horizontal"  method="post" action="{{url('/register')}}">
					<input type="hidden"  class="form-control" name="_token" value="{{ csrf_token() }}">
					<div class="form-group has-error">
						<label class="col-sm-4 control-label">Kullanıcı Adı giriniz:</label>
						<div class="col-sm-8"><input type="text"  class="form-control mm1"  name="username" required></div>
					</div>
					<div class="form-group has-error">
						<label class="col-sm-4 control-label">Adınızı giriniz:</label>
						<div class="col-sm-8"> 
							<input type="text"  class="form-control mm1" name="name" required>
						</div>
					</div>
					<div class="form-group has-error">
						<label class="col-sm-4 control-label">Soyadınızı giriniz: </label>
						<div class="col-sm-8">
							<input type="text"  class="form-control mm1" name="surname" required>
						</div>
					</div>
					<div class="form-group has-error">
						<label class="col-sm-4 control-label">E-mail Adresinizi giriniz: </label>
						<div class="col-sm-8">
							<input type="email"  class="form-control mm1" name="email" required>
						</div>
					</div>
					<div class="form-group has-error">
						<label class="col-sm-4 control-label">Şifre giriniz: </label>
						<div class="col-sm-8 ">
							<input type="password"  class="form-control mm1" name="password" required>
						</div>
					</div>
					<div class="form-group has-error">
						<label class="col-sm-4 control-label">Şifreyi tekrar giriniz: </label>
						<div class="col-sm-8 ">
							<input type="password" name="passwordrepeat"  class="form-control mm1" required>
						</div>
					</div>
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-danger">Gönder</button>
					</div>
			</form>
@endsection