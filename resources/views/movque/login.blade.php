@extends('movque.master')
@section('content')
<div class="container">
<div class="panel  panel-danger col-md-5 kgiris ppanel1">
    <div class="panel-heading"><i class="fa fa-cube" aria-hidden="true"></i> PROJE İSİMLERİ </div>
	    <div class="panel-body">
			<form class="form-horizontal" method="post" action="{{url('/login')}}">
				<input type="hidden" class="form-control" name="_token" value="{{ csrf_token() }}">
				<div class="form-group has-error">
					<label class="col-sm-4 control-label">E-mail Adresi:</label>
					<div class="col-sm-8">
					<input type="email" class="form-control mm1" name="email" required>
					</div>
				</div>
				<div class="form-group has-error">
					<label class="col-sm-4 control-label">Şifre:</label>
					<div class="col-sm-8">
					<input class="form-control mm1" type="password" name="password" required></label>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-danger">Gönder</button>
					</div>
				</div>
			</form>
		</div>
</div>
</div>
@endsection