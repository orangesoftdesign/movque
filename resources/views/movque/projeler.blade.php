@extends('movque.master')
@section('content')
<div class="container">
<!-- Görev Listele -->
    <div class="panel panel-danger">
      <div class="panel-heading"><i class="fa fa-cube" aria-hidden="true"></i> PROJE İSİMLERİ </div>
      <!-- Table -->
        <table class="table">
          <thead>
            <th>Proje İsmi</th>
          </thead>
          <tbody>
          @foreach($projes as $key => $value)
          <tr>
              <td><a href="{{url('/projeler/'.$value->project_id)}}">{{$value->project_name}}</a></td>
          </tr>
          @endforeach
          </tbody>
        </table>
        <div class="panel-footer">Presented by: TEAM MOVQ</div>
    </div>
</div>
@endsection
