@extends('movque.master')
@section('content')

<div class="container">

  <div class="profilcover" style="background-image: url({{url($user->coverPicture)}})">
      <img src="{{url($user->profilePicture)}}" alt="K.E.P." class="img-circle profilavatar">
      <div class="ppname">
        <a href="{{url('/profil')}}" class="pname"><span>{{$user->name." ".$user->surname}}</span></a>
        <span class="text-muted small"><em> {{ "@".$user->username }}</em></span>
      </div>

    @if(Auth::user()->username === $user->username)
    <a href="{{url('/profilduzenle')}}"><span class="label label-default" style="position: absolute;margin-top: 335px;margin-left: 220px;">Düzenle</span></a>
    @endif
     
    <div style="position: absolute;margin-top: 335px;margin-left: 200px;">
        @if(Auth::user()->role_id === 2 && $user->role_id != 2)
        <form method="post" action="">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="userID" value="{{ $user->id }}">
          <div class="col-md-8">
            <select class="form-control" name="newrole_id">
              <option value=0>Üye</option>
              <option value=1>Moderator</option>
              <option value=2>Admin</option>
            </select>
          </div>
          <button type="submit" name="yetkidegistir" value="Yetki Değiştir" class="btn btn-danger col-md-4">Kaydet</button>
        </form>
        @endif
    </div>
</div>

<!-- Görev Listele -->
    <div class="panel panel-danger">
      <div class="panel-heading"><i class="fa fa-cube" aria-hidden="true"></i> PROJE İSİMLERİ </div>
      <!-- Table -->
        <table class="table">
          <thead>
            <th>Proje İsmi</th>
            <th>Görevler</th>
          </thead>
          <tbody>
          @foreach($projeler as $key => $value)
          <tr>
              <td>{{$value->project_name}}</td>
              <td>{{$value->gorev}}</td>
          </tr>
          @endforeach
          </tbody>
        </table>


        <div class="panel-footer">Presented by: Kadir Emre Parlak</div>
    </div>
</div>

@endsection
