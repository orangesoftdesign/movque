@extends('movque.master')
@section('content')
<div class="container">
  <div class="profilcover" style="background-image: url('{{ Auth::user()->coverPicture  }}')">
  <img src="{{ Auth::user()->profilePicture  }}" alt="K.E.P." class="img-circle profilavatar">
  <div class="ppname"><a href="{{url('/profil')}}" class="pname"><span>{{Auth::user()->name." ".Auth::user()->surname}}</span></a><span class="text-muted small"><em> {{ "@".Auth::user()->username }}</em></span>
  </div><a href="{{url('/profilduzenle')}}"><span class="label label-default" style="position: absolute;margin-top: 335px;margin-left: 220px;">Düzenle</span></a>
  </div>
  
        <!-- Görev Listele -->
            <div class="panel panel-danger">
              <div class="panel-heading"><i class="fa fa-cube" aria-hidden="true"></i> PROJE İSİMLERİ </div>
              <!-- Table -->
                  <table class="table table-bordered table-striped">
                    <thead>
                      <th>Devam Eden Projeler</th>
                    </thead>
                    <tbody>
                      @foreach($devam_projeler as $key => $value)
                        <tr>
                        
                            <td><a href="{{url('/projeler/'.$value->project_id)}}">{{$value->project_name}}</a></td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                <table class="table table-bordered table-striped">
                  <thead>
                    <th>Biten Projeler</th>
                  </thead>
                  <tbody>
                    @foreach($biten_projeler as $key => $value)
                      <tr>
                          <td><a href="{{url('/projeler/'.$value->project_id)}}">{{$value->project_name}}</a></td>
                      </tr>
                   @endforeach
                  </tbody>
                </table>
                <div class="panel-footer">Presented by: TEAM MOVQ</div>
            </div>
  </div>
@endsection
