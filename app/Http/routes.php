<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/', function () {
    return view('movque.anasayfa');
});
Route::get('/register', 'mainController@register');
Route::post('/register', 'mainController@registertosend');
Route::get('/projeler/{id}', 'mainController@projeyegit');
Route::post('/projeler/{id}', 'mainController@projeguncelle');
Route::get('/projeler', 'mainController@projelistele');
Route::post('/projeler', 'mainController@projeolustur');
Route::post('/profilduzenle', 'mainController@updateProfile'); 
Route::get('/profil/{username}','gorevController@profilGoster'); 

Route::post('/gorevolustur', 'gorevController@gorevkayit');
Route::get('/gorevsil/{id}', 'gorevController@gorevsil');
Route::post('/profil', 'gorevController@gorevkayit');
Route::get('/profil', 'gorevController@profil');
Route::get('/profilgoster/{username}', 'gorevController@profilGoster');

Route::get('/profilduzenle', 'gorevController@profilduzenle');
Route::get('/login', 'mainController@loginPage'); 
Route::post('/login', 'mainController@login'); 
Route::get('/logout', 'mainController@logout'); 

Route::get('/kullaniciduzenle', 'adminController@kullaniciDuzenlePage');
Route::post('/profil/{username}', 'adminController@yetkiDuzenle');
Route::post('/profilgoster/{username}', 'adminController@yetkiDuzenle');