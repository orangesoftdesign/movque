<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Projeler;
use App\Gorev;

use File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Hashing\BcryptHasher;



class mainController extends Controller
{
    public function register(){
        return view('movque.register');
    }

    public function loginPage()
    {
        return view('movque.login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
    public function projelistele(){ 
        $gonder["projes"] = Projeler::orderBy('project_id','DESC')->where('goster','=','0')->get(); 
        $gonder["projes"] = Projeler::orderBy('project_id','DESC') 
        ->where('goster','=','0') 
        ->get(); 
        return view('movque.projeler', $gonder); 
    } 
    //  Bu fonksiyonda proje oluşturmak için yazıldı.
    public function projeolustur(Request $request){

            if(!Auth::check())
                return redirect('login');

                $control = $request->proje;
                if (!empty($control)) {
                    for($i=0; $i<sizeof($control); $i++){

                        if((Auth::user()->role_id == 2))
                        {
                            $proje = new Projeler;
                            $proje -> project_name = $request -> proje;
                            $proje -> owner_id = Auth::user()->id;
                            $proje-> modID = $request->modID;
                            $proje -> save();
                            return redirect()->to('/profil');
                        }
                    }
                return redirect()->back();
                }
                else{
                return redirect()->back(); 
            }
        } 
    // Bu fonksiyonda projenin ismi tamamlanıp tamamlanmadığı ve Moderatorun belirlenmesi işlemi gerçekleştirildi.
    public function projeguncelle(Request $request,$id){
       
        $projeguncelle = Projeler::where('project_id',$id)->update([
        'project_name' => $request -> proje,
        'modID' => $request -> modID,
        'goster' => $request -> goster,
        ]);        
        return redirect()->back(); 

    }

    //  Bu fonksiyonda Profil sayfasında projeye tıklandığında farklı birsayfada açılması yapıldı.
    public function projeyegit($projeid){


        $data["proje"] = Projeler::where('project_id',$projeid)
        ->first();

        $data["projeler"] = Gorev::where('project_id',$projeid)
        ->where('goster',0)
        ->get();

        $data["modlist"] = User::where("role_id","=","1")
        ->get();

         $data["mod"] = User::where("id","=",$data["proje"]->modID)
        ->get();



        return view('movque.gorevler',$data);
    }
    
    public function registertosend(Request $request){

        // Yeni kullanıcı kaydı
        $kullanici = new User;
        $kullanici ->username = $request->input('username');
        $kullanici ->name = $request->input('name');
        $kullanici ->surname = $request->input('surname');
        $kullanici ->email =$request->input('email');
        $kullanici ->role_id = 0;
        $usernameControl = User::where('username', '=', $request->input('username'))
        ->where('email', '=', $request->input('email'))
        ->get();


        if (count($usernameControl) == 0) {
        if(strcmp($request->input('password') , $request->input('passwordrepeat')) == 0)
        {
            $kullanici ->password = bcrypt($request->input('password'));
            $kullanici -> save();
        }
        }
        else{

        Log::error('Lütfen başkabir kullanıcıadı veya email giriniz.');
        }
        return redirect() -> back();
    }

    public function updateProfile(Request $request){

        // Kullaniciyi bul
        $kullanici = User::find(Auth::user()->id);



        // Kullanıcı adı, ad, soyad, email değiştir
            if(null !== $request->input('username'))
        $kullanici ->username = $request->input('username');

            if(null !== $request->input('name'))
        $kullanici ->name = $request->input('name');

            if(null !== $request->input('surname'))
        $kullanici ->surname = $request->input('surname');

            if(null !==$request->input('email'))
        $kullanici ->email =$request->input('email');

        // Şifre değişikliği isteği geldiyse, yeni şifre 6 karakterden uzunsa, eski şifre doğru girildiyse şifreyi değiştir
        if(strcmp($request->input('newpassword') , $request->input('newpasswordrepeat')) == 0)
        if(null !==$request->input('newpassword'))
            if(strlen($request->input('newpassword'))>=6)
                if(password_verify($request->input('password'), $kullanici->password))
                    $kullanici ->password = bcrypt($request->input('newpassword'));

        // Profil fotoğrafı değişikliği isteği geldiyse profil fotoğrafını değiştir
        if(Input::hasFile('image')){

            $file=Input::file('image');
            $filename=time().rand(0,5000).$file->getClientOriginalName();
            $file->move('uploads' ,$filename);

            if(strcmp($kullanici->profilePicture,"uploads/user.png") != 0)
                File::delete($kullanici->profilePicture);

            $kullanici->profilePicture = "uploads/".$filename;
        }

        // Kapak fotoğrafı değişikliği isteği geldiyse kapak fotoğrafını değiştir
        if(Input::hasFile('imageK')){

            $file=Input::file('imageK');
            $filename=time().rand(0,5000).$file->getClientOriginalName();
            $file->move('uploads' ,$filename);

            if(strcmp($kullanici->coverPicture,"uploads/defaultCover.jpg") != 0)
                File::delete($kullanici->coverPicture);

            $kullanici->coverPicture = "uploads/".$filename;
        }
        // Değişiklikleri veritabanına kaydet
        $kullanici -> save();
        return redirect() -> back();
    }
    public function login(Request $request)
    {
        $uye = array(
            'email' => $request->input('email'),
            'password' => $request->input('password')
        );
        if (\Auth::attempt($uye))
        {
            return redirect("/");
        }
        else
        {
            echo  "E-mail / şifre yanlış";
            return redirect('login');
        }
    }
}
