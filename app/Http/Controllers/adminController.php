<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Gorev;
use App\Projeler;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Hashing\BcryptHasher;



class adminController extends Controller
{
    // Kullanıcı düzenle sayfasını açar. Her bir kullanıcının çalıştığı görevler & projeler listelenir.
    public function kullaniciDuzenlePage(){
    $uyeler = User::orderBy('role_id','DESC')->where('role_id',0)->get();
    $moderator = User::orderBy('role_id','DESC')->where('role_id',1)->get();
    $admin = User::orderBy('role_id','DESC')->where('role_id',2)->get();
    $projeler = Projeler::orderBy('project_id','DESC')->get();
    $gorevler = Gorev::orderBy('id','DESC')->get();
 
    if (Auth::user()->role_id == 2) {
    return view('movque.kullaniciduzenle', compact('admin','moderator','uyeler','projeler','gorevler')); 
    }else{
    return view('movque.hata');
    }

    }

 

    // Kullanıcının yetkisini (role_id) düzenler.
    public function yetkiDuzenle(Request $request){

        //dd($request);


        // Kullaniciyi bul
        $kullanici = User::find($request->input('userID'));

        $kullanici->role_id = $request->input('newrole_id');

        // Değişiklikleri veritabanına kaydet
        $kullanici -> save();
        return redirect() -> back();
    }
   

}
