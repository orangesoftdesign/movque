<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Gorev;
use App\User; 
use App\Projeler;

class gorevController extends Controller {
	// Bu fonksiyonda profilde projeler listelenirken aynı zamanda gorevler tablosu ile projeler tablosu birleştirildi.
	public function profil(){
		if(!Auth::check()) 
		return redirect('login');

		$projeler = Projeler::orderBy('id','DESC')
				->join('gorevler', 'gorevler.project_id', '=', 'projects.project_id')
				->groupby('project_name')->distinct()
				->get(['gorevler.project_id', 'gorevler.gorev', 'gorevler.id', 'projects.project_name', 'projects.goster']);

		$devam_projeler = Projeler::where('goster',0)->where('owner_id','=',Auth::user()->id)->orderBy('project_id', 'DESC')->get();
		$biten_projeler = Projeler::where('goster',1)->get();

				return view('movque.profil')->with('projeler', $projeler)->with('devam_projeler', $devam_projeler)->with('biten_projeler', $biten_projeler);

	}
	// Bu fonksiyonda profil düzenleme işlemi gerçekleştirildi.
	public function profilduzenle(){
		if(!Auth::check()) 
      return redirect('login'); 

		return view('movque.profilduzenle');
	}
	// Bu fonksiyonda gorevkayıt işlemi gerçekleştirildi.
	public function gorevkayit(Request $request ){
		

		if(!Auth::check()) 
		return redirect('login');


		$prj = Projeler::where('project_id','=',$request->project_id)->get();

        
		$control = $request->gorev;
		// Giriş yapan kullanıcı admin ise veya projenin moderatörü ise görev kaydını gerçekleştir.
		if (!empty($control) || (Auth::user()->role_id == 2 || (Auth::user()->role_id == 1 && $prj->modID === Auth::user()->id))) {
			for($i=0; $i<sizeof($control); $i++){
				$gorev = new Gorev;
				$gorev -> gorev = $request -> gorev[$i];
				if(strlen($request -> gorev[$i])>0)
				$gorev -> user_id = Auth::user()->id;
				$gorev -> owner_id = Auth::user()->id;
				$gorev -> project_id = $request->project_id;
				$gorev -> save();
			}
			
			return redirect()->back();
		}
		else{
			return redirect() -> back();
		}
	}

	// Bu fonksiyonda görev listeleme işlemi gerçekleştirildi.
	public function gorevlistele(){
		if(!Auth::check()) 
		return redirect('login');

		$gonder["gorevs"] = Gorev::orderBy('id','DESC')->where('goster','=','0')->get();
		return view('movque.gorevlistele', $gonder);

	}
	// Bu fonksiyonda görev silme işlemi gerçekleştirildi.
	public function gorevsil($gid){
		if(!Auth::check()) 
		return redirect('login');

		Gorev::where('id', '=', $gid)
			->update(['goster' => true]);
		return redirect() -> back();

	}
	// Bu fonksiyonda kullanıcıların profillerinin gösterilmesi işlemi ve profilde gorevlerin listelenmesi işlemi gerçekleştirildi.
	public function profilGoster($username){ 
    if(!Auth::check()) 
    return redirect('login'); 
 
    $user = User::where("username","=",$username)->first();

    $projeler = Projeler::orderBy('id','DESC')
        ->join('gorevler', 'gorevler.project_id', '=', 'projects.project_id')
        ->where('gorevler.user_id', $user->id) 
        ->get(['gorevler.project_id', 'gorevler.gorev', 'gorevler.id', 'projects.project_name']); 
 
    return view('movque.profilgoster', compact('user','projeler')); 
  } 




}