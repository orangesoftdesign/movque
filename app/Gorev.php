<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gorev extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gorevler';
}